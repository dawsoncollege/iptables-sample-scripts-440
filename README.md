# sample scripts for iptables manipulation
This is an administrive task so must be done with root privileges.  Be careful with these scripts as some will open your firewall to access from the outside.

Also note that these refer to IPv4 only

## Before you do anything, backup
* On ubuntu, using ufw take a copy of /etc/ufw/* 
* on any use (I usually append a date to the file name by `$(date +%F)`)
  1.  Save: iptables-save > save.rules
  1.  Restorie: iptables-restore < save.rules

### ONLY USE THESE in a lab or on an isolated system
use to 
* flush the existing rules while testing iptabes [iptables-flush.sh](iptables-flush.sh)
* as a skeleton to apply your own rules, put your rule sets in `# insert your commands here` s while testing iptabes [iptables-skel.sh](iptables-skel.sh)
* to apply logging of all ICMP traffic, in general logging can slow your access, so you will not want to see it on for very long.  [logging-iptables.sh](logging-iptables.sh)

### On Fedora flush the netmanager extra chains
These are specific to Fedora (last checked v 20) you will haVe to remove ufw chains on Ubuntu.
* flush filter table [del-netmanager-chains.sh](del-netmanager-chains.sh)
* flush nat table [del-netmanager-nat-chains.sh](del-netmanager-nat-chains.sh)

