#!/bin/bash
# PMCampbell
# iptables-skel.sh
# December 2010/April 2013/May 2014

# Skeleton script for iptables testing
# if you want to use this file download it and
# make it executable and add your own rule sets 
# where you see "# insert  your commands here"

# flush filter table
iptables -F 

#n.b. set default policy  "that which is not allowed is denied"
iptables -P INPUT DROP 

# need this as first rule for stateful filtering:  estab & related:
iptables -A INPUT -p all -m state --state ESTABLISHED,RELATED  -j ACCEPT 

# you can use this as the base of an iptables script
# add the rules you need here
# insert  your commands here
# insert  your commands here
# insert  your commands here
# insert  your commands here

# you will sometimes see, instead of a default policy a last rule
# but it is positional & can end up in the wrong place or be omitted
# this is here so you can see the reject response, but normally would be omitted
# and rely on the Default policy of drop
iptables -A INPUT -j REJECT
#end of program
