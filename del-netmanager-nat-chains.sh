#!/bin/bash
#flush nat table, then delete NetManager chains
# fedora 20 NetManager chains

iptables -t nat  -F
iptables -t nat -F
iptables -t nat --delete-chain OUTPUT_direct
iptables -t nat --delete-chain POSTROUTING_ZONES
iptables -t nat --delete-chain POSTROUTING_ZONES_SOURCE
iptables -t nat --delete-chain POSTROUTING_direct
iptables -t nat --delete-chain POST_external
iptables -t nat --delete-chain POST_external_allow
iptables -t nat --delete-chain POST_external_deny
iptables -t nat --delete-chain POST_external_log
iptables -t nat --delete-chain POST_public
iptables -t nat --delete-chain POST_public_allow
iptables -t nat --delete-chain POST_public_deny
iptables -t nat --delete-chain POST_public_log
iptables -t nat --delete-chain PREROUTING_ZONES
iptables -t nat --delete-chain PREROUTING_ZONES_SOURCE
iptables -t nat --delete-chain PREROUTING_direct
iptables -t nat --delete-chain PRE_public
iptables -t nat --delete-chain PRE_public_allow
iptables -t nat --delete-chain PRE_public_deny
iptables -t nat --delete-chain PRE_public_log       
