#!/bin/bash
# PMCampbell
# iptables-flush.sh
# December 2017/May  2014 
# April 2019, added warning
# April 2021 added flush non-builtin chains
# Skeleton script for iptables testing
# if you want to use this file download it and
# make it executable and run before testing your own rule sets 

# flush everything
echo "WARNING about to blow your firewall wide open"
echo "do not use this on an internet connected computer, kill if necessary" input
read -p "do you want to contine?  type yes: " input
if [[ $input != yes ]] ; then
    echo "$(basename $0) no authorization to continue"
    exit 5
fi
fn=/tmp/iptables-before-flush.$(date +%F-h_%H_%M_%S)
iptables-save >  $fn
echo "Backup (for your own good) in $fn"
echo "To restore use: iptables-restore < $fn"
echo
echo "About to flush tables"
# flush all tables
iptables -t nat -F
iptables -t mangle -F
iptables -t raw -F
iptables -F

echo "Opening default policies for filter table chains "
# filter table chains, default policy: accept
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT

echo "Opening default policies for mangle table chains"
# mangle table chains,  default policy: accept 
iptables -t mangle -P PREROUTING ACCEPT
iptables -t mangle -P FORWARD ACCEPT
iptables -t mangle -P INPUT ACCEPT
iptables -t mangle -P OUTPUT ACCEPT
iptables -t mangle -P POSTROUTING ACCEPT

echo "Opening default policies for nat table chains"
# nat table chains,  default policy: accept
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P OUTPUT ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

echo "Delete all non-builtin chains"
# the below will flush all non-builtin chains
iptables -X 
